#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using std::cout;
using std::endl;
using std::string;

int carAvailable = 0;
int carPrice = 0;
int accountsBalance = 0;


int carAvailableCheck(void* notUsed, int argc, char** argv, char** azCol)
{
	carAvailable = std::stoi(argv[argc - 1], nullptr, 10);		//Last Col is Available
	if (carAvailable)		//if Car available then rememmber the price too
	{
		carPrice = std::stoi(argv[argc - 2], nullptr, 10);		//the Col that is one from the end is Price
	}
	return 0;
}

int getAccountsBalance(void* notUsed, int argc, char** argv, char** azCol)
{
	accountsBalance = std::stoi(argv[argc - 1], nullptr, 10);
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	string command = "";
	carAvailable = 0;
	carPrice = 0;
	accountsBalance = 0;

	command = " select * from cars where id=" + std::to_string(carid);
	rc = sqlite3_exec(db, command.c_str(), carAvailableCheck, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else if (carAvailable == 0)
	{
		return 0;
	}

	command = " select * from accounts where buyer_id=" + std::to_string(buyerid);
	rc = sqlite3_exec(db, command.c_str(), getAccountsBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else if (carPrice>accountsBalance)
	{
		return 0;
	}
	else
	{
		rc = sqlite3_exec(db, "begin transaction", NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		command = " update accounts set balance =" + std::to_string((accountsBalance - carPrice)) + " where buyer_id=" + std::to_string(buyerid);
		rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		command = " update cars set available = 0 where id=" + std::to_string(carid);
		rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		rc = sqlite3_exec(db, "commit", NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		return 1;
	}

}


bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = 0, sourceAcc = 0, destAcc = 0;
	string command = "";
	accountsBalance = 0;

	command = " select * from accounts where id=" + std::to_string(from);
	rc = sqlite3_exec(db, command.c_str(), getAccountsBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	else
	{
		sourceAcc = accountsBalance;
	}

	command = " select * from accounts where id=" + std::to_string(to);
	rc = sqlite3_exec(db, command.c_str(), getAccountsBalance, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}
	else
	{
		destAcc = accountsBalance;
	}

	if ((sourceAcc - amount) < 0)
	{
		return 0;
	}

	rc = sqlite3_exec(db, "begin transaction", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}

	command = " update accounts set balance =" + std::to_string((sourceAcc - amount)) + " where buyer_id=" + std::to_string(from);
	rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}

	command = " update accounts set balance =" + std::to_string((destAcc + amount)) + " where buyer_id=" + std::to_string(to);
	rc = sqlite3_exec(db, command.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}

	rc = sqlite3_exec(db, "commit", NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 0;
	}

	return 1;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	cout << carPurchase(12, 1, db, zErrMsg) << endl;
	cout << carPurchase(3, 7, db, zErrMsg) << endl;
	cout << carPurchase(9, 20, db, zErrMsg) << endl;


	system("Pause");
	system("CLS");

	cout << balanceTransfer(1, 2, 30000, db, zErrMsg) << endl;
	cout << balanceTransfer(4, 6, 80000, db, zErrMsg) << endl;
	cout << balanceTransfer(12, 7, 200000, db, zErrMsg) << endl;

	system("Pause");
	return 0;
}
